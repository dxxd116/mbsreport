package report.solr
/**
  * Created by cai on 10/11/2016 for MBSReport.
  */
import java.io.FileWriter

import com.redis.RedisClient

import scala.io.Source
/**
  * Created by cai on 10/6/2016.
  */
class MBSCellTowers {

  def getCellTowerIdsRedis={

    val shapes =List("mbs002","mbs003","mbs004","mbs005","mbs006","mbs007")
    val fw=new FileWriter("src/main/resources/cellTowers.txt")
    //val fw=new FileWriter("D:/work/singtel/MBS/results/cellTowers.txt")
    val fw2=new FileWriter("src/main/resources/newCellTowers.txt")
//    val shapes =List("mbs002")
//    val fw=new FileWriter("D:/work/singtel/MBS/results/cellTowers-Hotel.txt")
    val redis=new RedisClient("54.179.189.48",6379)
    for ( keyString: String <- shapes) {
      val list = redis.smembers(keyString)
      println(list.get.size + " cell towers for " + keyString)
      list.get.foreach(x => {fw.write(x.getOrElse("") + "\r\n");fw2.write("525-1-"+x.getOrElse("") + "\r\n"); })
    }

    redis.disconnect
    fw.close()
    fw2.close()
  }

  def getCellTowerIdsFile(cellType:String):List[String]= {

    //val shapes =List("mbs002","mbs003","mbs004","mbs005","mbs006","mbs007")
    //val shapes =List("mbs002")
    var fileName: String = "/cellTowers.txt";
    if (cellType.equalsIgnoreCase("new")) {
      fileName="/newCellTowers.txt"
    }
     val source = Source.fromInputStream(this.getClass().getResourceAsStream(fileName ))
      source.getLines().toList
    }





}

object MBSCellTowers{

  def main(args:Array[String])={
    val obj=new MBSCellTowers();
    obj.getCellTowerIdsRedis
    //val cellTowers=obj.getCellTowers
    //getCellTowers("old").foreach(println)


  }

  def getCellTowers(cellType:String):List[String]={
    val obj=new MBSCellTowers()
    obj.getCellTowerIdsFile(cellType)

  }
}

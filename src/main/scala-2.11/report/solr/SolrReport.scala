package report.solr

import java.io.{FileInputStream, FileWriter}
import java.util.{ArrayList, Date, Properties}

import org.apache.commons.lang3.StringUtils
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.solr.client.solrj.{SolrQuery, SolrRequest}
import org.apache.solr.common.util.SimpleOrderedMap
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import utils.SolrUtils

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Created by cai on 10/10/2016.
  */
class SolrReport {
    var solrClient:CloudSolrClient=null
    var properties:Properties=null


  def this(config:Properties)={
    this
    this.setProperties(config)
  }

  def setProperties(config:Properties)={
    this.properties=config
    val zkhost=config.getProperty("zkhost")
    solrClient=SolrUtils.getCloudSolrClient(zkhost)
  }

  def setEnv(config:Properties)={
    //val zkhost=config.getProperty("zkhost");
    //"52.76.80.91,52.76.58.188,52.76.76.186:2181"
    //solrClient=SolrUtils.getCloudSolrClient(zkhost)
  }

  def getHourlyImsi(dateTime:DateTime)={
    //val solrUtils=new SolrUtils()
    //SolrUtils.setEnv("internet")
    val hourlyStats=new mutable.HashMap[String,Long]()

    val inputDir=properties.getProperty("input.dir")
    val outputDir=properties.getProperty("output.dir")


    val cellTowerFile=inputDir + "cellTowers.txt"
    val cellTowers=getCellTowers(cellTowerFile)
    println(cellTowers.size + " cell towers found!")
    val cellTowerFilterString="cell_tower_s:(\"" + StringUtils.join(cellTowers.asJava,"\" \"")+ "\")"
    val allFilterString="duration_ti:[1800 TO *] AND " + cellTowerFilterString;

    val dateTimeFormatter=DateTimeFormat.forPattern("yyyyMMdd")
    val collectionName="footfall"+dateTimeFormatter.print(dateTime);

    val solrQuery=new SolrQuery("*:*");
    solrQuery.add("fq",allFilterString);

    val dateTimeFormatter2=DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
    val startTimeStr=dateTimeFormatter2.print(dateTime)
    val endTimeStr=dateTimeFormatter2.print(dateTime.plusDays(1))
    val jsonFacet=
      "{hourlyImsis:{type:range,field:visit_start_tdt,start:\""+ startTimeStr+ "\", end: \"" +  endTimeStr +
      "\", gap:\"+1HOUR\", facet:{uniqueImsis:{type:query,q:'imsi:*',domain:{blockParent:'imsi:*'},facet:{uniqueImsis:'hll(imsi)'}}} }}"
    //println(jsonFacet)
    solrQuery.set("json.facet",jsonFacet);
    solrQuery.set("collection",collectionName)

    println(solrQuery.toString)

    val solrResponse=solrClient.query(collectionName,solrQuery,SolrRequest.METHOD.POST)
    val namedList=solrResponse.getResponse.findRecursive("facets","hourlyImsis")
    val bucketList=namedList.asInstanceOf[SimpleOrderedMap[Object]].findRecursive("buckets").asInstanceOf[ArrayList[SimpleOrderedMap[Object]]]

    for (bucket:SimpleOrderedMap[Object] <- bucketList.asScala){
      val timestamp=dateTimeFormatter2.print(new DateTime(bucket.get("val").asInstanceOf[Date]));

      val uniqueImsis=bucket.get("uniqueImsis").asInstanceOf[SimpleOrderedMap[Long]]
      val imsiCount=uniqueImsis.get("count")
      hourlyStats.+= (timestamp -> imsiCount)
    }
    println(solrResponse.getResults.getNumFound + " documents in total!")

    hourlyStats.toList.sortBy(x => x._1.substring(8,13)).foreach(x => println(x._1 +"=>\t"+x._2))


    val fileName = outputDir+"hourlyUniqueImsis-"+dateTimeFormatter.print(dateTime) + ".log"
    writeOutputToFile(fileName,hourlyStats)
  }

  def writeOutputToFile(fileName:String,hourlyStats:mutable.HashMap[String,Long])={
    val fw=new FileWriter(fileName);
    hourlyStats.toList.sortBy(x => x._1.substring(8,13)).foreach(x => fw.write(x._1+", " + x._2+"\r\n"))
    fw.close()
  }
  def getCellTowers(filename:String):List[String]={
    var lines:ListBuffer[String]=new ListBuffer[String]();
    val src=Source.fromFile(filename);
    src.getLines().foreach( x => lines.+=(x) )
    lines.toList
  }

  def cleanUp()={
    solrClient.close()
  }
}


object SolrReport{

  def main(args:Array[String])={

    if (args.length ==0){
      println("Usage: java -jar <jarFile> <config.properties>");
      System.exit(1)
    }
    val configFile=args(0)

    val properties=new Properties()
    properties.load(new FileInputStream(configFile))
    //val tool=new SolrReport()
    //tool.setProperties(properties)
    val tool=new SolrReport(properties)

    val startDateStr=args(1)
    val endDateStr=args(2)
    val inputDateFormatter=DateTimeFormat.forPattern("yyyyMMdd" )

    val startDate= inputDateFormatter.parseDateTime(startDateStr)
    val endDate= inputDateFormatter.parseDateTime(endDateStr)

    var dateTime:DateTime=startDate
    while (!dateTime.isAfter(endDate))
    {
        tool.getHourlyImsi(dateTime)
        dateTime=dateTime.plusDays(1)
    }

    tool.cleanUp()

//    val dateTime=new DateTime(2016,8,1,0,0)
//    tool.getHourlyImsi(dateTime)
//    val dateTimeAfter=new DateTime(2016,8,25,0,0)
//    tool.getHourlyImsi(dateTimeAfter)
//    val cellTowers=tool.getCellTowers(fileName)
//    println(cellTowers.size + " cell towers found!")
  }
}
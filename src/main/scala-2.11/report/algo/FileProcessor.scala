package report.algo

import java.io.{File, FileInputStream}
import java.util.Properties

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.format.DateTimeFormat
import report.solr.MBSCellTowers

import scala.collection.mutable


/**
  * Created by cai on 10/13/2016 for MBSReport.
  */
class FileProcessor {

    var cellTowers=List[String]()
    val outputFormatter=DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:00:00'Z'");
    val inputFormatter=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

  def setCellTowers(cellType:String)={
    cellTowers=MBSCellTowers.getCellTowers(cellType)
  }

def countUnique(record:(String,Iterable[String]))={
    var uniqueImsis=new mutable.HashSet[String]()
    val iter=record._2
  for (x <- iter){
    if (!uniqueImsis.contains(x)){
      uniqueImsis.add(x)
    }
  }

  record._1 + ", "+ uniqueImsis.size
}

def mapRecord(line:String,delimiter:String):(String,String)={
  val parts=line.split(delimiter)
  val imsi=parts(0)
  val cellTowerid=parts(1)
  val startTime=inputFormatter.parseDateTime(parts(2))

  //val endTime=inputFormatter.parseDateTime(parts(3))
  val startHourStr=outputFormatter.print(startTime)

  (startHourStr,imsi)
}

  def filterRecord(line:String,delimiter:String):Boolean={
    val parts=line.split(delimiter)
    val imsi=parts(0)
    val cellTowerid=parts(1)
    val startTime=inputFormatter.parseDateTime(parts(2))
    val endTime=inputFormatter.parseDateTime(parts(3))
    val duration=(endTime.getMillisOfDay-startTime.getMillisOfDay)/1000

    if (cellTowers.contains(cellTowerid) && duration >= 1800){true} else false

  }

}

object FileProcessor{
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val conf=new SparkConf().setAppName("Local file processing").setMaster("local[2]")
  val sc=new SparkContext(conf)
  val obj =new FileProcessor
  var properties=new Properties()

  def main(args:Array[String])={
    if (args.size <3 ) {
      println("Usage: java -jar <jarfile> <inputFilePath> <outputFilePath>");
      System.exit(1)
    }

    val configFile=args(0)
    getProperties(configFile)
    val delimiter=properties.getProperty("text.delimiter")

    sc.getConf.setMaster(properties.getProperty("spark.master"))
    val cellType=properties.getProperty("celltower.type")
    obj.setCellTowers(cellType)

    val cellMap=MBSCellTowers.getCellTowers(cellType).toSet[String]
    println(cellMap.size + " cell towers found!\n")
    //cellMap.foreach(x => println(x) )

    val s3accessKeyId=properties.getProperty("awsAccessKeyId")
    val s3accessKey=properties.getProperty("awsSecretAccessKey")
    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", "AKIAI2JMT4TQPIL7MV7A")
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey","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")
    if (s3accessKey!=null && s3accessKeyId!=null){
      sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", s3accessKeyId)
      sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey",s3accessKey)
    }

    val inputFileName=args(1)
    val outputFileName=args(2)

    processDataCS(inputFileName,outputFileName,delimiter)
    sc.stop()
  }

  def getProperties(fileName:String)={
    this.properties.load(new FileInputStream(fileName))
  }

  def processDataCS(fileName:String,outputFileName:String,delimiter:String)= {
    val rdd1 = sc.textFile(fileName)
    val filteredRdd = rdd1.filter(x => obj.filterRecord(x,delimiter))
    println(filteredRdd.count() + " records found!")
    val mappedRDD = filteredRdd.map(x => obj.mapRecord(x,delimiter)).groupByKey()
    val finalRDD = mappedRDD.map(x => obj.countUnique(x))

    if (! new File(outputFileName).exists()) {
      //finalRDD.saveAsTextFile(outputFileName, classOf[GzipCodec])
      finalRDD.saveAsTextFile(outputFileName)
    }
    else{
      System.out.println("The directory already exists!")
      finalRDD.collect().foreach(println)
      //Files.delete(Paths.get(outputFileName))
      //finalRDD.saveAsTextFile(outputFileName)
    }

  }


}
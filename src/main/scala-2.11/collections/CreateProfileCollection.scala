package collections

import java.io.{FileInputStream, IOException}
import java.util.Properties

import org.apache.log4j.{Level, Logger}
import org.apache.solr.client.solrj.SolrServerException
import utils.{CollectionUtils, SolrUtils}

/**
  * Created by cai on 11/5/2016 for MBSReport.
  */
object CreateProfileCollection {


  private val properties=new Properties()
  private val logger=Logger.getLogger(CreateProfileCollection.getClass)

  def main(args: Array[String]):Unit= {

    if (args.length == 0) {
      logger.error("Usage : java -cp MBSReport.jar collections.CreateProfileColleciton <config-profile.properties> <collectionName>")
      System.exit(1)
    }
    val startTime = System.currentTimeMillis()
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    val configFile = args(0)
    val collectionName=args(1)

    properties.load(new FileInputStream(configFile))
    val sparkMaster = properties.getProperty("spark.master")

    val zkHost = properties.getProperty("zkhost")
    val nodeSet=properties.getProperty("nodeSet")
    val configName=properties.getProperty("configName")
    val numShards=properties.getProperty("numShards").toInt
    val numReplicas =properties.getProperty("numReplicas").toInt
    //val maxShardsPerNode=properties.getProperty("maxShardsPerNode")

    val solrClient=SolrUtils.getCloudSolrClient(zkHost)
    try {
      CollectionUtils.createCollection(solrClient, collectionName, configName, nodeSet, numShards, numReplicas)
    } catch {
      case e:SolrServerException => logger.error(e.getMessage)
      case e:IOException          => logger.error(e.getMessage)
      case e:Throwable                => logger.error(e.getMessage)
    }


    solrClient.close()

  }

}

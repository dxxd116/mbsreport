package collections

import java.io.{FileInputStream, IOException}
import java.util.Properties

import org.apache.log4j.{Level, Logger}
import org.apache.solr.client.solrj.SolrServerException
import org.joda.time.format.DateTimeFormat
import utils.{CollectionUtils, SolrUtils}

/**
  * Created by cai on 11/6/2016 for MBSReport.
  */
object CreateFootfallCollections {


  private val properties=new Properties()
  private val currentDateTimePattern=DateTimeFormat.forPattern("YYYYMMdd")
  private val logger=Logger.getLogger(this.getClass)

  def main(args: Array[String]):Unit= {

    if (args.length == 0) {
      logger.error("Usage : java -cp MBSReport.jar collections.CreateFootfallColleciton <config-footfall.properties> <startDate>")
      System.exit(1)
    }
    val startTime = System.currentTimeMillis()
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    val configFile = args(0)
    val startDate=currentDateTimePattern.parseDateTime(args(1))
    val endDate=startDate.plusMonths(1)

    properties.load(new FileInputStream(configFile))
    val sparkMaster = properties.getProperty("spark.master")

    val zkHost = properties.getProperty("zkhost")
    val nodeSet=properties.getProperty("nodeSet")
    val configName=properties.getProperty("configName")
    val numShards=properties.getProperty("numShards").toInt
    val numRepplicas =properties.getProperty("numReplicas").toInt
    //val maxShardsPerNode=properties.getProperty("maxShardsPerNode")

    val solrClient=SolrUtils.getCloudSolrClient(zkHost)
    var collectionDate=startDate
    while (collectionDate.isBefore(endDate)) {
      val collectionName="footfall"+ currentDateTimePattern.print(collectionDate)
      logger.info(collectionName + " is to be created!")
      try {
        CollectionUtils.createCollection(solrClient, collectionName, configName, nodeSet, numShards, numRepplicas)
      } catch {
        case e:SolrServerException  => logger.error(e.getMessage)
        case e:IOException          => logger.error(e.getMessage)
        case e:Throwable            => logger.error(e.getMessage)
    }
      collectionDate=collectionDate.plusDays(1)
    }
    solrClient.close()

  }

}

package indexing

import java.io.FileInputStream
import java.util
import java.util.{Properties, UUID}

import org.apache.http.impl.client.SystemDefaultHttpClient
import org.apache.log4j.{Level, Logger}
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.solr.common.SolrInputDocument
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import utils.{CollectionUtils, S3Utils, SolrUtils}

/**
  * Created by cai on 10/26/2016 for MBSReport.
  */
object FootfallSingapore {

  private var solrClient:CloudSolrClient=null;
  private val inputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
  private val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
  private val currentDateTimePattern=DateTimeFormat.forPattern("YYYYMMdd")
  private var delimiterFootfall="\\|";
  private val properties=new Properties()
  private val logger=Logger.getLogger(FootfallSingapore.getClass)

  def main(args: Array[String])={

    if (args.size ==0 ) {
      logger.error("Usage : java -cp MBSReport.jar indexing.FootfallSingapore <config.properties> <inputFile> <collectionName>")
      System.exit(1);
    }
    val startTime=System.currentTimeMillis();
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    val configFile=args(0);

    properties.load(new FileInputStream(configFile))
    val sparkMaster=properties.getProperty("spark.master")
    this.delimiterFootfall=properties.getProperty("text.delimiter.footfall")
    val zkHost=properties.getProperty("zkhost")

    val fileName=args(1)
    val collectionName=args(2)
    val dateStr=collectionName.substring(8,16)
    if (!checkS3Folder("footfall",dateStr)){

      logger.error("Folder "+dateStr+" is empty! Please upload data to S3 bucket first!")
      System.exit(1);
    }


    solrClient=getCloudSolrClient(zkHost)
    if (!CollectionUtils.existsCollection(solrClient,collectionName)){
      logger.error("Collection "+collectionName+" is not available yet!")
      System.exit(1)
    }
    solrClient.deleteByQuery(collectionName,"*:*");
    solrClient.commit(collectionName,true,true);
    //solrClient.close();

    val conf=new SparkConf().setAppName("Footfall Indexing Job")

    logger.info("Spark master is set to " + sparkMaster)
    conf.setMaster(sparkMaster);

    val sc=new SparkContext(conf);
    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", "AKIAJBVIMIDJNVLN3TQQ")
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey","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")
    val s3accessKeyId=properties.getProperty("awsAccessKeyId")
    val s3accessKey=properties.getProperty("awsSecretAccessKey")
    if (s3accessKey!=null && s3accessKeyId!=null){
      sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", s3accessKeyId)
      sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey",s3accessKey)
    }

    val lines=sc.textFile(fileName);


    val pairRdd=lines.map( x => (x.split(this.delimiterFootfall)(0),  x))
    val groupedRdd=pairRdd.reduceByKey((x,y) => x + "==" + y);
    //pairRdd.take(50).foreach(a => println(a._1 + "\t=>" + a._2));
    //groupedRdd.take(50).foreach(a => println(a._1 + "=>\t"+a._2.mkString("||")));

    logger.info(groupedRdd.partitions.length + " partitions in total!")
    logger.info("Start to index documents for "+collectionName+"...");

    try {
      groupedRdd.foreachPartition(x => indexJob(zkHost, collectionName, x))
    }
    catch {
      case e:Throwable => logger.error(e.getMessage)
    }

    logger.info("Start to optimizing Index!")
    val t1=System.currentTimeMillis()
    //solrClient=getCloudSolrClient(zkHost)
    solrClient.optimize(collectionName,true,true);
    val t2=(System.currentTimeMillis()-t1)/1000
    logger.info(t2 + " seconds in optimization!")

    solrClient.close();
    val endTime=System.currentTimeMillis();
    logger.info((endTime-startTime)/1000 + " seconds in indexing " + collectionName + "!");

    sc.stop()
  }


  def getCloudSolrClient(zkhost: String):CloudSolrClient={
    val cl = new SystemDefaultHttpClient();
    val solrClient=new CloudSolrClient(zkhost,cl);
    return solrClient;
  }

  def indexJob(zkHost:String,collectionName:String,datas:Iterator[(String,String)]):Unit={
    val solrClient=SolrUtils.getCloudSolrClient(zkHost);
    val documents=new util.ArrayList[SolrInputDocument]();

    while (datas.hasNext){
      val data=datas.next();
      val imsi  =data._1;
      val arr   =data._2;

      val masterDoc=new SolrInputDocument();
      masterDoc.addField("id",UUID.randomUUID().toString());
      masterDoc.addField("imsi",imsi);

      //var children=scala.collection.mutable.ListBuffer.empty[SolrInputDocument];
      var children=   getChildren(arr);
      masterDoc.addChildDocuments(children)

      documents.add(masterDoc);
      if (documents.size() >=30000){
        solrClient.add(collectionName,documents);
        //solrClient.commit(collectionName,true,true);
        documents.clear();
        println("30000 documents indexed!");
      }
    }


    if (documents.size()>0){
      solrClient.add(collectionName,documents);
      solrClient.commit(collectionName,true,true);
    }

    solrClient.close()

  }

  def getChildren(combinedLine:String):util.ArrayList[SolrInputDocument]={
    var children=new util.ArrayList[SolrInputDocument]();
    val lines=combinedLine.split("==")
    for (line <- lines){
      val parts=line.split(this.delimiterFootfall,8);

      val imsi=parts(0);
      /*Remove the country code and telco code from cell tower Id
      525-1-344-9582
      344-9582
      */

      val areaId=parts(1).substring(6);
      val startDate=inputDatetimePattern.parseDateTime(parts(2));
      val endDate=inputDatetimePattern.parseDateTime(parts(3));
      var submtzId=0
      var subzoneId=0;
      var planAreaId=0;
      var planRegionId=0;
      if (!parts(4).isEmpty) {
        submtzId = parts(4).toInt;
        subzoneId = parts(5).toInt
        planAreaId = parts(6).toInt
        planRegionId = parts(7).toInt
      }

      val duration=endDate.secondOfDay().get()-startDate.secondOfDay().get();

      val dates=getDatesBetween(startDate,endDate);
      val stayDates=dates._1;
      val hours=dates._2;

      val child =new SolrInputDocument();
      child.addField("id",UUID.randomUUID().toString());
      child.addField("cell_tower_s",areaId);
      child.addField("submtz_ti",submtzId)
      child.addField("subzone_ti",subzoneId)
      child.addField("plan_area_ti",planAreaId)
      child.addField("plan_region_ti",planRegionId)
      child.addField("visit_start_tdt",outputDatetimePattern.print(startDate));
      child.addField("visit_end_tdt",outputDatetimePattern.print(endDate));
      child.addField("visit_stay_tdts",stayDates);
      child.addField("hour_tis",hours);
      child.addField("duration_ti",duration)

      children.add(child);
    }
    return children;

  }


  def getDatesBetween( startDate :  DateTime, endDate:DateTime):(util.ArrayList[String],util.ArrayList[Int])={
    val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
    val dates=new util.ArrayList[String]();
    val hours=new util.ArrayList[Int];


    var datetime=startDate.minusMinutes(startDate.minuteOfHour().get()).minusSeconds(startDate.secondOfMinute().get())
    while (datetime.compareTo(endDate) <=0 ){
      hours add  datetime.hourOfDay().get()

      dates add outputDatetimePattern.print(datetime)
      datetime=datetime.plusHours(1)
    }
    return (dates,hours)
  }


  def convertDate(s:String):String={
    val dat=inputDatetimePattern.parseDateTime(s)
    //dat.secondOfMinute=0;
    val outputString=outputDatetimePattern.print(dat)

    return outputString;
  }

  def checkS3Folder(bucketName:String,folderPrefix:String):Boolean={
    val s3accessKeyId=properties.getProperty("awsAccessKeyId")
    val s3accessKey=properties.getProperty("awsSecretAccessKey")
    val s3tools=new S3Utils(s3accessKeyId,s3accessKey)

    val flag=s3tools.checkS3Folder(bucketName,folderPrefix);
    s3tools.closeClient()
    flag
  }


}

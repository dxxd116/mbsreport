package indexing

import java.io.FileInputStream
import java.util
import java.util.Properties

import com.vividsolutions.jts.geom.{Coordinate, GeometryFactory}
import org.apache.log4j.{Level, Logger}
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.solr.common.SolrInputDocument
import org.apache.spark.{SparkConf, SparkContext}
import utils.{CollectionUtils, S3Utils, SolrUtils}

import scala.collection.JavaConverters._
/**
  * Created by cai on 10/26/2016 for MBSReport.
  */
object ProfileLocalSingapore {
  private var solrClient:CloudSolrClient=_
  private var delimiterProfileDemograph="\\|"
  private var delimiterProfileHomeWork="\t"
  private val properties=new Properties()
  private val logger=Logger.getLogger(ProfileLocalSingapore.getClass)

  def main(args: Array[String]):Unit= {

    if (args.size == 0) {
      logger.error("Usage : java -cp MBSReport.jar indexing.ProfileLocalSingapore <config.properties> <Demographics FileName> <Homework FileName> <collectionName>")
      System.exit(1);
    }
    val startTime = System.currentTimeMillis();
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
/*
  Get Configurations from config properties file
 */
    val configFile = args(0);
    properties.load(new FileInputStream(configFile))
    val sparkMaster = properties.getProperty("spark.master")
    this.delimiterProfileDemograph = properties.getProperty("text.delimiter.profileDemograph")
    this.delimiterProfileHomeWork=properties.getProperty("text.delimiter.profileHomeWork")
    val zkHost = properties.getProperty("zkhost")
    val demoFile = args(1)
    val homeworkFile=args(2)
    val collectionName = args(3)

    solrClient = SolrUtils.getCloudSolrClient(zkHost)
    if (!CollectionUtils.existsCollection(solrClient,collectionName)){
      logger.error("Collection "+collectionName+" is not available yet!")
      System.exit(1)
    }
    solrClient.deleteByQuery(collectionName, "*:*");
    solrClient.commit(collectionName, true, true);

    val conf=new SparkConf().setAppName("Profile Local Indexing Job")

    logger.info("Spark master is set to " + sparkMaster)
    conf.setMaster(sparkMaster);

    val sc=new SparkContext(conf);
    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", "AKIAJBVIMIDJNVLN3TQQ")
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey","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")
    val s3accessKeyId=properties.getProperty("awsAccessKeyId")
    val s3accessKey=properties.getProperty("awsSecretAccessKey")
    if (s3accessKey!=null && s3accessKeyId!=null){
      sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", s3accessKeyId)
      sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey",s3accessKey)
    }

    if (!(checkS3Folder("profile-locals","homework") && checkS3Folder("profile-locals","demographics"))){
      logger.error("AWS folder is empty! Please upload the data first!")
      System.exit(1)
    }else {
      logger.info("Found data in S3 buckets! Proceed to index ...")
    }

    val demographs=sc.textFile(demoFile).map(x => {val parts=x.split(delimiterProfileDemograph,2); (parts(0),parts(1))})
    val homeworks=sc.textFile(homeworkFile).map(x => {val parts=x.split(delimiterProfileHomeWork,2); (parts(0),parts(1))})

    val joinedProfileLines=demographs.fullOuterJoin(homeworks,100)

    try {
      joinedProfileLines.foreachPartition(x => indexJob(zkHost, collectionName, x))
    }catch {
      case e:Throwable => logger.error(e.getMessage)
    }

    logger.info("Start to optimizing Index!")
    val t1=System.currentTimeMillis()
    //solrClient=getCloudSolrClient(zkHost)
    solrClient.optimize(collectionName,true,true);
    val t2=(System.currentTimeMillis()-t1)/1000
    logger.info(t2 + " seconds in optimization!")

    solrClient.close();
    val endTime=System.currentTimeMillis();
    logger.info((endTime-startTime)/1000 + " seconds in indexing "+collectionName+"!");

    sc.stop()
  }

  def indexJob(zkHost:String,collectionName:String,lines:Iterator[(String,(Option[String],Option[String]))]): Unit ={

    val solrUtils=new SolrUtils(zkHost)
    val solrClient=SolrUtils.getCloudSolrClient(zkHost)
    val documents=new util.ArrayList[SolrInputDocument]()
    val factory=new GeometryFactory();

    for (x <- lines
          ){
      val imsi=x._1
      val info=x._2
      val demographStr=info._1
      val demographs=demographStr match {
        case None => Array("NA","NA","NA","NA","NA","NA")
        case x:Some[String] => demographStr.get.split(delimiterProfileDemograph, 6)
      }
      val homeworks=info._2
      val parts=  homeworks match{
        case None => Array("NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA","NA")
        case x:Some[String] => x.get.split(delimiterProfileHomeWork)
      }
      //println(x._1 + "=\t=" + demographs + "=\t=" + homeworkLocs)

      val solrDoc=new SolrInputDocument();
      solrDoc.addField("imsi_s",imsi)
      solrDoc.addField("id",imsi)
      val msisdn=getString(demographs(0))
      val customerType=getCustomerTypeString(demographs(1))
      val nationality=getString(demographs(2))
      val age =getInteger(demographs(3))
      val gender=getGender(demographs(4))
      val race=getString(demographs(5))
      val profileType="local"

      val homeAddress = getString(parts(0))
      val homeLocations=getLocations(homeAddress)
      val homeSubmtzId=homeLocations(0)
      val homeSubzoneId=homeLocations(1)
      val homePlanAreaId=homeLocations(2)
      val homePlanRegionId=homeLocations(3)
      val homeType=getString(parts(1))
      val homeMRT=getString(parts(2))
      val homeBuilding=getString(parts(3))

      val workAddress=getString(parts(4))
      val workLocations=getLocations(workAddress)
      val workSubmtzId=workLocations(0)
      val workSubzoneId=workLocations(1)
      val workPlanAreaId=workLocations(2)
      val workPlanRegionId=workLocations(3)

      val workMRT=getString(parts(5))
      val workBuilding=getString(parts(6))
      val occupationType=getString(parts(7))
      val dayTimePattern=getString(parts(8))
      val foreignWorker=getString(parts(9))
      val shiftWorker=getString(parts(10))

      solrDoc.addField("msisdn_s",msisdn)
      solrDoc.addField("customer_type_s",customerType)
      solrDoc.addField("nationality_s",nationality)
      solrDoc.addField("age_ti",age)
      solrDoc.addField("gender_s",gender)
      solrDoc.addField("race_s",race)
      solrDoc.addField("profile_type_s",profileType)

      solrDoc.addField("home_submtz_ti",homeSubmtzId)
      solrDoc.addField("home_subzone_ti",homeSubzoneId)
      solrDoc.addField("home_plan_area_ti",homePlanAreaId)
      solrDoc.addField("home_plan_region_ti",homePlanRegionId)
      solrDoc.addField("home_type_s",homeType)
      solrDoc.addField("home_MRT_s",homeMRT)
      solrDoc.addField("home_building_s",homeBuilding)

      solrDoc.addField("work_submtz_ti",workSubmtzId)
      solrDoc.addField("work_subzone_ti",workSubzoneId)
      solrDoc.addField("work_plan_area_ti",workPlanAreaId)
      solrDoc.addField("work_plan_region_ti",workPlanRegionId)
      solrDoc.addField("work_MRT_s",workMRT)
      solrDoc.addField("work_building_s",workBuilding)
      solrDoc.addField("w_occupation_s",occupationType)
      solrDoc.addField("daytime_pattern_s",dayTimePattern)
      solrDoc.addField("foreignWorker_s",foreignWorker)
      solrDoc.addField("shiftWorker_s",shiftWorker)

      documents.add(solrDoc)
    }

    solrClient.add(collectionName,documents,10000)

    solrClient.close()

    def getLocations(s:String):List[Integer]={
      if (s=="Unknown")  { List(-11111,-11111,-11111,-11111) }
      else {
        val parts=s.split(",",2)
        val longitude=parts(1).toDouble
        val latitude=parts(0).toDouble
        val point=factory.createPoint(new Coordinate(longitude,latitude))
        val submtzId=solrUtils.findSubmtzByPoint(point)
        val locations=solrUtils.getHierarchyIds(submtzId)

        locations.asScala.toList
      }
    }


  }



  def getGender(s:String)={
    s match {
      case "NA" => "Unknown"
      case "M"  => "male"
      case "F"  => "female"
    }
  }
  def getString(s:String):String={
    s match {
    case "NA" => "Unknown"
    case x:String => x
  }
}
  def getCustomerTypeString(s:String):String={
    s match {
      case "NA" => "Unknown"
      case x:String => x.toLowerCase
    }
  }

  def getInteger(s:String):Int={
    s match {
      case "NA" => -11111
      case x:String => x.toInt
    }
  }

  def getDouble(s:String):Double={
    s match {
      case "NA" => -11111.0
      case x:String => x.toDouble
    }

  }


  def checkS3Folder(bucketName:String,folderPrefix:String):Boolean={
      val s3accessKeyId=properties.getProperty("awsAccessKeyId")
      val s3accessKey=properties.getProperty("awsSecretAccessKey")
      val s3tools=new S3Utils(s3accessKeyId,s3accessKey)

      val flag=s3tools.checkS3Folder(bucketName,folderPrefix);
      s3tools.closeClient()
      flag
  }


//  def getCloudSolrClient(zkhost: String):CloudSolrClient={
//    val cl = new SystemDefaultHttpClient();
//    val solrClient=new CloudSolrClient.Builder().withZkHost(zkhost).withHttpClient(cl).build();
//    return solrClient;
//  }
}

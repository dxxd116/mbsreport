package indexing

import java.io.FileInputStream
import java.util
import java.util.Properties

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.vividsolutions.jts.geom.{Coordinate, GeometryFactory}
import org.apache.log4j.{Level, Logger}
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.solr.common.SolrInputDocument
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import utils.{CollectionUtils, S3Utils, SolrUtils}

import scala.collection.JavaConverters._
/**
  * Created by cai on 11/1/2016 for MBSReport.
  */
object ProfileVisitorSingapore {

  private var solrClient:CloudSolrClient=_
  private val inputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  private val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'")
  private var delimiterProfileVisitors="\t"
  private val properties=new Properties()
  private val logger=Logger.getLogger(ProfileVisitorSingapore.getClass)
  private val countryList=getNationalities()


  def main(args: Array[String]):Unit= {

    if (args.length.==(0)) {
      logger.error("Usage : java -cp MBSReport.jar indexing.ProfileVisitorSingapore <config.properties>  <Visitor FileName> <collectionName>")
      System.exit(1)
    }
    val startTime = System.currentTimeMillis()
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)
 /*
  Get Configurations from config properties file
 */
    val configFile = args.apply(0)
    properties.load(new FileInputStream(configFile))
    val sparkMaster = properties.getProperty("spark.master")
    this.delimiterProfileVisitors = properties.getProperty("text.delimiter.profileVisitors")
    val zkHost = properties.getProperty("zkhost")
    val visitorFile = args.apply(1)
    val collectionName = args.apply(2)


    solrClient = SolrUtils.getCloudSolrClient(zkHost)
    if (!CollectionUtils.existsCollection(solrClient,collectionName)){
      logger.error("Collection "+collectionName.+(" is not available yet!"))
      System.exit(1)
    }

    val conf=new SparkConf().setAppName("Profile Visitor Indexing Job")

    logger.info("Spark master is set to " + sparkMaster)
    conf.setMaster(sparkMaster)

    val sc=new SparkContext(conf)
    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", "AKIAJBVIMIDJNVLN3TQQ")
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey","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")
    val s3accessKeyId=properties.getProperty("awsAccessKeyId")
    val s3accessKey=properties.getProperty("awsSecretAccessKey")
    if (s3accessKey!=null && s3accessKeyId!=null){
      sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", s3accessKeyId)
      sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey",s3accessKey)
    }

    if (! checkS3Folder("profile-visitors","")){
      logger.error("AWS folder is empty! Please upload the data first!")
      System.exit(1)
    }else {
      logger.info("Found data in S3 buckets! Proceed to index ...")
    }

    val visitors=sc.textFile(visitorFile).map(x => {val parts=x.split(delimiterProfileVisitors,2); (parts.apply(0),parts.apply(1))})
    val uniqueVisitors=visitors.reduceByKey((x,y) => x + "||" + y)
    try {
      uniqueVisitors.foreachPartition(x => indexJob(zkHost, collectionName, x))
    } catch{
      case e:Throwable  => logger.error(e.getMessage)
    }

    logger.info("Start to optimizing Index!")
    val t1=System.currentTimeMillis()
    //solrClient=SolrUtils.getCloudSolrClient(zkHost)
    solrClient.optimize(collectionName,true,true)
    val t2=(System.currentTimeMillis()-t1)/1000
    logger.info(t2 + " seconds in optimization!")

    solrClient.close()
    val endTime=System.currentTimeMillis()
    logger.info((endTime-startTime)/1000 + " seconds in indexing "+collectionName+"!")

    sc.stop()
  }

  def indexJob(zkHost:String,collectionName:String,lines:Iterator[Tuple2[String, String]]): Unit = {

    val solrUtils = new SolrUtils(zkHost)
    val solrClient = SolrUtils.getCloudSolrClient(zkHost)
    val documents = new util.ArrayList[SolrInputDocument]()
    val factory = new GeometryFactory()

    lines.foreach(x => {
      val imsi = x._1
      val text = x._2
      val trips = text.split("\\|\\|")
      var tripCount: Int = 0
      var tripDuration: Int = -1
      var totalTripDuration: Int = 0
      var arrivalDateStr: String = ""
      var entryPoint = ""
      var entryType = ""
      var departureDateStr: String = ""
      var exitPoint = ""
      var exitType = ""
      var country = ""
      var tripPurpose = ""
      var tripTypes = new util.ArrayList[String]()
      var topHangout = ""


      trips.foreach(trip => {
        val parts = trip.split(delimiterProfileVisitors)
        val currentArrivalDate = getDate(parts.apply(0))
        val currentEntryPoint = getString(parts.apply(1))
        val currentEntryType = getString(parts.apply(2))
        val currentDepartureDate = getDate(parts.apply(3))
        val currentExitPoint = getString(parts.apply(4))
        val currenntExitType = getString(parts.apply(5))
        val currentTripDuration = getInteger(parts.apply(6))
        country = getString(parts.apply(7))
        val currentTripPurpose = getString(parts.apply(8))
        val currentTripTypes = getTripTypes(parts.apply(9))
        val currentTopHangout = getString(parts.apply(12))

        totalTripDuration = totalTripDuration + currentTripDuration
        tripCount = tripCount + 1
        if (tripDuration < currentTripDuration) {
          arrivalDateStr = outputDatetimePattern.print(currentArrivalDate)
          entryPoint = currentEntryPoint
          entryType = currentEntryType
          departureDateStr = outputDatetimePattern.print(currentDepartureDate)
          exitPoint = currentExitPoint
          exitType = currenntExitType
          tripDuration = currentTripDuration
          tripPurpose = currentTripPurpose
          tripTypes = currentTripTypes
          topHangout = currentTopHangout
        }
      })

      val hangoutLocations = getLocations(topHangout)
      val topHangoutSubmtzId = hangoutLocations.head
      val topHangoutSubzoneId = hangoutLocations.apply(1)
      val topHangoutPlanAreaId = hangoutLocations.apply(2)
      val topHangoutPlanRegionId = hangoutLocations.apply(3)

      val solrDoc = new SolrInputDocument()
      solrDoc.addField("id", imsi)
      solrDoc.addField("imsi_s", imsi)
      solrDoc.addField("country_s", countryList.getOrElse(country, "Unknown"))
      solrDoc.addField("arrival_tdt", arrivalDateStr)
      solrDoc.addField("entry_point_s", entryPoint)
      solrDoc.addField("entry_type_s", entryType)
      solrDoc.addField("departure_tdt", departureDateStr)
      solrDoc.addField("exit_point_s", exitPoint)
      solrDoc.addField("exit_type_s", exitType)
      solrDoc.addField("trip_count_ti", tripCount)
      solrDoc.addField("trip_duration_td", totalTripDuration)

      solrDoc.addField("trip_purpose_s", tripPurpose)
      solrDoc.addField("trip_type_ss", tripTypes)
      solrDoc.addField("topHangout_submtz_ti", topHangoutSubmtzId)
      solrDoc.addField("topHangout_subzone_ti", topHangoutSubzoneId)
      solrDoc.addField("topHangout_plan_area_ti", topHangoutPlanAreaId)
      solrDoc.addField("topHangout_plan_region_ti", topHangoutPlanRegionId)

      solrDoc.addField("profile_type_s", "visitor")
      documents.add(solrDoc)
    })

    solrClient.add(collectionName,documents,10000)
    solrClient.close()

    def getLocations(s:String):List[Integer]={
      if (s=="Unknown" )  List.apply(-11111,-11111,-11111,-11111)
      else {

        val parts=s.split(",",2)

        val longitude=parts.apply(1).toDouble
        val latitude=parts.apply(0).toDouble

        val point=factory.createPoint(new Coordinate(longitude,latitude))
        val submtzId=solrUtils.findSubmtzByPoint(point)
        val locations=solrUtils.getHierarchyIds(submtzId)

        locations.asScala.toList
      }
    }
  }

  def getTripTypes(s:String):util.ArrayList[String]={
    s match {
      case "NA" => new util.ArrayList[String]()
      case x:String => {
        val arrs = new util.ArrayList[String]()
        x.split("\\|").foreach(x => arrs.add(x))
        arrs
      }
    }
  }

  def getString(s:String):String={
    s match {
      case "NA" => "Unknown"
      case x:String => x
    }
  }

  def getDate(s:String):DateTime={
    s match {
      case "NA" => null
      case x:String => inputDatetimePattern.parseDateTime(x)
    }
  }

  def getInteger(s:String):Int={
    s match {
      case "NA" => -11111
      case x:String => x.toInt
    }
  }

  def getDouble(s:String):Double={
    s match {
      case "NA" => -11111.0
      case x:String => x.toDouble
    }

  }


  def checkS3Folder(bucketName:String,folderPrefix:String):Boolean={
    val s3accessKeyId=properties.getProperty("awsAccessKeyId")
    val s3accessKey=properties.getProperty("awsSecretAccessKey")
    val s3tools=new S3Utils(s3accessKeyId,s3accessKey)

    val flag=s3tools.checkS3Folder(bucketName,folderPrefix)
    s3tools.closeClient()
    flag
  }


  def getNationalities(): Map[String,String] ={

    //val fileName=properties.getProperty("nationalityLookupTable")
    //val inputStream=new FileReader(fileName)
    val inputStream=this.getClass.getResourceAsStream("/nationality_lookup")

    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    val countries=mapper.readValue(inputStream,classOf[Map[String,String]])
    println("Papua New Guinea" + "=>" + countries.get("Papua New Guinea"))

    logger.info(countries.keySet.size + " countries found!")
    inputStream.close()
    countries
  }

}



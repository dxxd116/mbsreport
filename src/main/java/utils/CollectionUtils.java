package utils;

import org.apache.commons.io.FileUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.util.NamedList;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class CollectionUtils {

    public static void main(String[] args) throws IOException, SolrServerException{
        //CloudSolrClient client = new CloudSolrClient("52.74.153.22:2181");//Singapore
        CloudSolrClient client = new CloudSolrClient("52.76.76.186,52.76.80.91,52.76.58.188:2181");//Singapore
        //CloudSolrClient client = new CloudSolrClient("52.65.54.15,52.65.103.71,52.65.103.73:2181");//Optus
        //String nodeSet="ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-174-110.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-61-235.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-140-23.ap-southeast-1.compute.amazonaws.com:8983_solr,52.74.232.128:8983_solr,52.74.43.159:8983_solr";
        //nodeSet="ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr";
        DateTime startDate=new DateTime(2016,11,15,0,0);
        DateTime endDate=new DateTime(2016,11,15,0,0);
        String nodeSet="ec2-52-77-140-23.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-174-110.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-61-235.ap-southeast-1.compute.amazonaws.com:8983_solr";

        for (;!startDate.isAfter(endDate);startDate=startDate.plusDays(1)){
            String collectionName="footfall" +String.format("%4d%02d%02d", startDate.getYear(),startDate.getMonthOfYear(),startDate.getDayOfMonth());
            //System.out.println("Deleting documents in " + collectionName);
            //client.deleteByQuery(collectionName, "*:*");
            //client.commit(collectionName, true, true);

            //optimizeCollection(client,collectionName);

            createCollection(client,collectionName,"footfall_cell_tower",nodeSet,4,1);
            //createCollection(client,collectionName,"building_config",nodeSet,3,1);
            //deleteCollection(client,collectionName);
        }

        //checkCollections(client,2016,9);
        //Create collections
        //createSingleCollection(client,"profile_Q1_2014","profile_config","52.76.86.78:8983_solr");
        //createSingleCollection(client,"profile_Q2_2014","profile_config","52.76.86.78:8983_solr");
        //createSingleCollection(client,"profile_Q3_2014","profile_config","52.76.86.78:8983_solr");
        //createSingleCollection(client,"profile_Q4_2014","profile_config","52.76.86.78:8983_solr");

        //createProfileCollection(client,"profile-Q1-2017","profile_locals_config",nodeSet,4,1);

        //deleteCollection(client, "profile-Q3-2016");
        //deleteCollection(client, "profile_Q3_2014");
        //deleteCollection(client, "profile_Q4_2014");

        //Optimize profile collections
			/*
			reloadSingleCollection(client,"profile_Q1_2014");
			reloadSingleCollection(client,"profile_Q2_2014");
			reloadSingleCollection(client,"profile_Q3_2014");*/
        //reloadSingleCollection(client,"profile-Q4-2016");

        //Add replicas on URA platform
			/*String node2="ec2-52-77-140-23.ap-southeast-1.compute.amazonaws.com:8983_solr";
			String node3="ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr";
            String node4="ec2-52-77-61-235.ap-southeast-1.compute.amazonaws.com:8983_solr";

            addReplicaToCollection(client,"profile-Q4-2016",node2);
			addReplicaToCollection(client,"profile-Q4-2016",node3);
			addReplicaToCollection(client,"profile-Q4-2016",node4);*/

			/*addReplicaToCollection(client,"profile_Q2_2014",node2);
			addReplicaToCollection(client,"profile_Q2_2014",node3);
			addReplicaToCollection(client,"profile_Q2_2014",node4);
			*/
        //Delete Replica
        //deleteReplicaFromCollection(client,"profile-Q4-2016","core_node3");
        //deleteReplicaFromCollection(client,"profile-Q4-2016","core_node4");

        //optimizeCollection(client,"profile-Q4-2016");
        //client.commit();
        client.close();
    }

    public static void deleteCollection(CloudSolrClient client,String collectionName) throws SolrServerException, IOException {
        // TODO Auto-generated method stub

        CollectionAdminRequest.Delete deleteReq = new CollectionAdminRequest.Delete();
        deleteReq.setCollectionName(collectionName);
        client.request(deleteReq);
        //client.commit(collection,true, true);
        System.out.println(collectionName + " deleted. \n");

    }

    public static void optimizeCollection(CloudSolrClient client, String collectionName) throws SolrServerException, IOException {
        // TODO Auto-generated method stub
        UpdateResponse res= client.optimize(collectionName,true,true);
        System.out.println(res.getStatus()+ "\t"+res.getQTime() + " ms query time!");
        System.out.println("Optimized collection - " + collectionName + "...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void deleteDocs(CloudSolrClient client, String collectionName) throws SolrServerException, IOException {
        // TODO Auto-generated method stub
        client.deleteByQuery(collectionName, "*:*");
        client.commit(collectionName,true,true);

    }

    public static void createCollection (SolrClient client, String collection, String configName, String nodeSet,int numShards, int numReplicas) throws IOException, SolrServerException {

        try {
            SolrRequest listReq=new CollectionAdminRequest.List() ;
            NamedList<Object> qr=client.request(listReq);

            List<String> collections=(List<String>) qr.get("collections");
            if (collections.contains(collection)){

                CollectionAdminRequest.Delete deleteReq = new CollectionAdminRequest.Delete();
                deleteReq.setCollectionName(collection);
                client.request(deleteReq);
                //client.commit(collection,true, true);
                System.out.println(collection + " deleted. \n");

            }
            else
            {
            }

        } catch (SolrException e) {e.printStackTrace();}


        System.out.println("Creating collection " + collection + " ... ");
        CollectionAdminRequest.Create createReq = new CollectionAdminRequest.Create();
        createReq.setConfigName(configName);
        createReq.setNumShards(numShards);
        createReq.setCreateNodeSet(nodeSet);

        createReq.setMaxShardsPerNode(1);
        createReq.setReplicationFactor(numReplicas);
        createReq.setCollectionName(collection);

        client.request(createReq);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        client.commit(collection,true,true);
    }






    public void createCore(String solrDir, String solrCore, SolrClient server) throws IOException, SolrServerException {
        File dest = new File(new File(solrDir), solrCore);
        File conf = new File(dest, "conf");
        if (!conf.exists()) {
            File src = new File(new File(solrDir), "configsets/data_driven_schema_configs/subdocConf");
            FileUtils.copyDirectory(src, conf);
            CoreAdminRequest.createCore(solrCore, new File(new File(solrDir), solrCore).getAbsolutePath(), server);

        }
    }

    public static void addReplicaToCollection (SolrClient client, String collection, String nodeName) throws IOException, SolrServerException {

        try {
            SolrRequest listReq=new CollectionAdminRequest.List() ;
            NamedList<Object> qr=client.request(listReq);

            List<String> collections=(List<String>) qr.get("collections");
            if (collections.contains(collection)){

            }
            else
            {
                System.out.println("Collection " + collection + " is not found!");
            }

        } catch (SolrException e) {System.exit(1);}


        System.out.println("Adding replica to collection " + collection + " ... ");
        CollectionAdminRequest.AddReplica req = new CollectionAdminRequest.AddReplica();
        req.setCollectionName(collection);
        req.setNode(nodeName);
        req.setShardName("shard1");


        client.request(req,collection);

        client.commit(collection,true,true);
    }



    public static void deleteReplicaFromCollection (SolrClient client, String collection, String nodeName) throws IOException, SolrServerException {

        try {
            SolrRequest listReq=new CollectionAdminRequest.List() ;
            NamedList<Object> qr=client.request(listReq);

            List<String> collections=(List<String>) qr.get("collections");
            if (collections.contains(collection)){

            }
            else
            {
                System.out.println("Collection " + collection + " is not found!");
                return;
            }

        } catch (SolrException e) {System.exit(1);}


        System.out.println("Deleting replica from collection " + collection + " ... ");
        CollectionAdminRequest.DeleteReplica req = new CollectionAdminRequest.DeleteReplica();
        req.setCollectionName(collection);
        req.setReplica(nodeName);
        req.setShardName("shard1");


        client.request(req,collection);

        client.commit(collection,true,true);
    }
    public static void checkCollections(CloudSolrClient client, Integer year,Integer month) throws SolrServerException, IOException {
        //Singapore
        //String zkhost="52.76.76.186,52.76.80.91,52.76.58.188:2181";
        //Optus
        //zkhost="52.65.54.15,52.65.103.71,52.65.103.73:2181";

        CollectionAdminRequest request=new CollectionAdminRequest.List();

        NamedList<Object> namedList=client.request(request);

        List<String> collections=(List<String>) namedList.findRecursive("collections");

        HashMap<String,Long> corruptCollections=new HashMap<String,Long>();

        FileWriter fw=new FileWriter("D:/work/singtel/files/corruptCollections-Singapore-"+year+".csv");
        collections.sort((x,y) -> x.compareTo(y));
        int i=0;
        for (String collection:collections)
        {

            String monthStr=String.format("%02d",month);
            if (!collection.startsWith("footfall"+year+monthStr)  )
            {
                continue;
            }

            i++;
            //System.out.println(i + "\t" + collection);

            try {
                SolrQuery query=new SolrQuery("*:*");
                QueryResponse qr=client.query(collection,query);
                SolrDocumentList docList=qr.getResults();
                long numFound=docList.getNumFound();

                System.out.println(numFound + " documents found in " + collection);

                if (numFound < 100){
                    corruptCollections.put(collection, numFound);
                    fw.write(collection + "," + numFound + "\r\n");
                }
            }
            catch (SolrServerException | IOException e)
            {
                e.printStackTrace();
            }
        }

        //ObjectMapper mapper=new ObjectMapper();
        //System.out.println(mapper.writeValueAsString(namedList));

        client.close();
        fw.close();
    }

    public static void reloadSingleCollection(CloudSolrClient client, String collectionName) {
        // TODO Auto-generated method stub
        CollectionAdminRequest.Reload reload=new CollectionAdminRequest.Reload();
        reload.setCollectionName(collectionName);
        try {
            NamedList<Object> res=client.request(reload,collectionName);
            if (res.findRecursive("success")!=null){
                System.out.println(collectionName + " reloaded successfully!");
            }
            else
            {
                System.out.println(collectionName + " is not reloaded !");
            }

        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static boolean existsCollection(CloudSolrClient client, String collectionName) throws IOException, SolrServerException {
        CollectionAdminRequest request=new CollectionAdminRequest.List();

        NamedList<Object> namedList=client.request(request);

        List<String> collections=(List<String>) namedList.findRecursive("collections");
        if (collections.contains(collectionName)){
            return true;
        }else {
            return false;
        }

    }

}
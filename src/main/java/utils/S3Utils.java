package utils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfilesConfigFile;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cai on 10/13/2016 for MBSReport.
 */
public class S3Utils {

    private  AmazonS3Client s3Client;

    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub

        S3Utils t1=new S3Utils("AKIAJBVIMIDJNVLN3TQQ","/V/6F3SU/ROb6rrkbrdWF8Y2xYN03iuZZjF83fmE");
        System.out.println("Getting S3 client!");
        //t1.setEnv("local");
        //t1.getLists();
        //t1.getLists("footfall");
        //t1.getLists("footfall","20161010");
        //t1.getTextFiles();

        //boolean flag=t1.checkS3Folder("footfall","20161025");
        //System.out.println(flag);
        t1.getSingleFile("footfall","20161001/part-00001.gz");
        //t1.getSingleFile("profile-visitors","inboundJulyDec2014.csv");
        //t1.getSingleFile("buildingvisit/DB_visits_buildings_20140203.txt.bz2");
        //t1.copySingleFile("profile/homework/homeworkQ1.csv","D:/work/singtel/files/profile-locals/homework_2014_Q1_test.csv");
        //t1.getSingleFile("areavisit/DB_visits_submtz_20141120.txt.bz2");
        //t1.getSingleFile("buildingvisit/DB_visits_buildings_20140301.txt.bz2");
        //t1.getSingleFile("buildingvisit/DB_visits_buildings_20140302.txt.bz2");

        //t1.uploadSingleFile("profile/profile_locals_details.txt");
        //t1.uploadSingleFile("hubbleapi.war");
        //t1.deleteSingleFile("hubbleapi.war");
    }

    public void setEnv(String type){

        if (type.equalsIgnoreCase("local"))
        {
            this.s3Client=getS3ClientLocal();
        }
        else
        {
            this.s3Client=getS3ClientEc2();
        }

    }
    public S3Utils(String accessKeyId,String awsSecretKey){
        this.s3Client=getS3ClientUsingKey(accessKeyId,awsSecretKey);
    }
    public S3Utils(){
        this.setEnv("local");
    }


    public void getSubmtzFiles() throws IOException
    {
        long t1=System.currentTimeMillis();
        List<String> keys=getLists();
        System.out.println(keys.size() + " files detected!");
        System.out.println("Start coping ...");
        String bucketName="ura-data";

        for (String key:keys){

            long ti1=System.currentTimeMillis();
            if (!key.startsWith("submtz/DB_visits_submtz_")){continue;}
            int pos=key.indexOf("/");
            String fileName=key.substring(pos+1);
            S3Object object = s3Client.getObject(
                    new GetObjectRequest(bucketName, key));
            InputStream is = object.getObjectContent();
            FileOutputStream output=new FileOutputStream(fileName);
            //Process the objectData stream.
            try {
                //BufferedReader br =new BufferedReader(new InputStreamReader(is,"UTF-8"));
                IOUtils.copy(is, output);
		/*int i=0;
		String line;
		while ((line=br.readLine())!=null)
		//for (i=0;i<1000;i++)
		{
			//line=br.readLine();
			writer.write(line);
		}*/
                //output.close();
                //is.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                output.close();
                is.close();
            }
            long ti2=System.currentTimeMillis();
            System.out.println(key + " is copied in " + Math.round((ti2-ti1)/1000) + " seconds!");
        }

        s3Client.shutdown();


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in coping contents!");

    }


    public void uploadSingleFile(String key) throws IOException
    {
        long t1=System.currentTimeMillis();

        System.out.println("Start copying file ..." + key);
        String bucketName="ura-data";
        String path="D:/work/singtel/files";
        //path="D:/SOLR/hubble_api/target";
        File file=new File(path,key);

        s3Client.putObject(bucketName, key, file);


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in total!");

    }

    public void deleteSingleFile(String key) throws IOException{
        long t1=System.currentTimeMillis();

        System.out.println("Start deleting file ..." + key);
        String bucketName="ura-data";
        s3Client.deleteObject(bucketName,key);
        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in deleting " + bucketName + "/" + key + "!");
    }

    public void getSingleFile(String key) throws IOException
    {
        long t1=System.currentTimeMillis();

        System.out.println("Start copying file ..." + key);
        String bucketName="ura-data";
        //String key="profile/subscribers.metadata.json";
        S3Object object = s3Client.getObject(
                new GetObjectRequest(bucketName, key));
        InputStream is = object.getObjectContent();
        //Process the objectData stream.
        //BufferedReader br =new BufferedReader(new InputStreamReader(is,"UTF-8"));


        FileOutputStream writer=new FileOutputStream("D:/work/singtel/files/" + key);
        try {
            IOUtils.copy(is,writer);
		/*int i=0;
		String line;
		//while ((line=br.readLine())!=null)
		for (i=0;i<1000000;i++)
		{
			//line=br.readLine();
			writer.write(line + "\n");
		}*/
            //writer.close();
            //is.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            writer.close();
            is.close();
        }

        //s3Client.shutdown();


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in coping contents!");

    }

    public void copySingleFile(String key,String localFileName) throws IOException
    {
        long t1=System.currentTimeMillis();

        System.out.println("Start copying file ..." + key);
        String bucketName="ura-data";
        //String key="profile/subscribers.metadata.json";
        S3Object object = s3Client.getObject(
                new GetObjectRequest(bucketName, key));
        InputStream is = object.getObjectContent();
        //Process the objectData stream.
        //BufferedReader br =new BufferedReader(new InputStreamReader(is,"UTF-8"));


        FileOutputStream writer=new FileOutputStream(localFileName);
        try {
            IOUtils.copy(is,writer);
		/*int i=0;
		String line;
		//while ((line=br.readLine())!=null)
		for (i=0;i<1000000;i++)
		{
			//line=br.readLine();
			writer.write(line + "\n");
		}*/
            //writer.close();
            //is.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            writer.close();
            is.close();
        }

        //s3Client.shutdown();


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in coping contents!");

    }
    public void getSingleFile(String bucketName,String key) throws IOException
    {
        long t1=System.currentTimeMillis();

        System.out.println("Start copying file ..." + key);
        //String bucketName="ura-data";
        //String key="profile/subscribers.metadata.json";
        S3Object object = s3Client.getObject(
                new GetObjectRequest(bucketName, key));
        InputStream is = object.getObjectContent();
        //Process the objectData stream.
        //BufferedReader br =new BufferedReader(new InputStreamReader(is,"UTF-8"));

        String fileName=key.replace('/','-' );
        FileOutputStream writer=new FileOutputStream("D:/work/singtel/files/" + bucketName+"/"+fileName);
        try {
            IOUtils.copy(is,writer);
		/*int i=0;
		String line;
		//while ((line=br.readLine())!=null)
		for (i=0;i<1000000;i++)
		{
			//line=br.readLine();
			writer.write(line + "\n");
		}*/
            //writer.close();
            //is.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            writer.close();
            is.close();
        }

        //s3Client.shutdown();


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in coping contents!");

    }
    public void getCsvFiles() throws IOException
    {
        long t1=System.currentTimeMillis();
        List<String> keys=getLists();
        System.out.println(keys.size() + " files detected!");
        System.out.println("Start coping ...");
        String bucketName="ura-data";

        for (String key:keys){

            long ti1=System.currentTimeMillis();
            if (!key.endsWith(".csv")){continue;}
            int pos=key.indexOf("/");
            String fileName=key.substring(pos+1);
            S3Object object = s3Client.getObject(
                    new GetObjectRequest(bucketName, key));
            InputStream is = object.getObjectContent();
            //Process the objectData stream.
            BufferedReader br =new BufferedReader(new InputStreamReader(is,"UTF-8"));
            FileWriter writer=new FileWriter(fileName);
            int i=0;
            String line;
            while ((line=br.readLine())!=null)
            //for (i=0;i<1000;i++)
            {
                //line=br.readLine();
                writer.write(line + "\n");
            }
            writer.close();
            is.close();

            long ti2=System.currentTimeMillis();
            System.out.println(key + " is copied in " + Math.round((ti2-ti1)/1000) + " seconds!");
        }

        //s3Client.shutdown();


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in coping contents!");

    }

    public void getTextFiles() throws IOException
    {
        long t1=System.currentTimeMillis();
        List<String> keys=getLists();
        System.out.println(keys.size() + " files detected!");
        System.out.println("Start coping ...");
        String bucketName="ura-data";

        for (String key:keys){

            long ti1=System.currentTimeMillis();
            if (!key.endsWith(".txt")){continue;}
            int pos=key.indexOf("/");
            String fileName=key.substring(pos+1);
            S3Object object = s3Client.getObject(
                    new GetObjectRequest(bucketName, key));
            InputStream is = object.getObjectContent();
            //Process the objectData stream.
            BufferedReader br =new BufferedReader(new InputStreamReader(is,"UTF-8"));
            FileWriter writer=new FileWriter(fileName);
            int i=0;
            String line;
            while ((line=br.readLine())!=null)
            //for (i=0;i<1000;i++)
            {
                //line=br.readLine();
                writer.write(line + "\n");
            }
            writer.close();
            is.close();

            long ti2=System.currentTimeMillis();
            System.out.println(key + " is copied in " + Math.round((ti2-ti1)/1000) + " seconds!");
        }

        //s3Client.shutdown();


        long t2=System.currentTimeMillis();
        System.out.println((t2-t1)/1000 + " seconds in coping contents!");

    }

    public boolean checkS3Folder(String bucketName,String folderPrefix){
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName).withPrefix(folderPrefix);

        Long totalSize=0L;
        ObjectListing objectListing;

        do {
            objectListing = s3Client.listObjects(listObjectsRequest);

            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries())
            {
                totalSize+=objectSummary.getSize();

//                System.out.println(String.format(objectSummary.getKey() + "  " +
//                        "(size = " + objectSummary.getSize()/1000 + "KB)\t" +
//                        "(Last Modified: " + String.format("%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS",objectSummary.getLastModified()) + ")" )
//                );
            }

            listObjectsRequest.setMarker(objectListing.getNextMarker());

        } while (objectListing.isTruncated());

        System.out.println(totalSize/1000 + " KB in total!");

        if (totalSize > 1000000) {
            return true;
        }else {
            return false;
        }

    }

    public List<String> getLists(String bucketName){
        return getLists(bucketName,"");
    }
    public List<String> getLists(String bucketName,String prefix)
    {
        long t1=System.currentTimeMillis();
        List<String> keys=new ArrayList<String>();
        long totalSize=0;
        //AmazonS3Client s3Client=getS3ClientLocal();
        System.out.println("Starting");
        //String bucketName="ura-data";
        //String key="submtz/DB_visits_submtz_20140426.csv";
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(bucketName);
        if (!prefix.isEmpty()){
            listObjectsRequest.withPrefix(prefix);
        }
        //.withPrefix("submtz");
        ObjectListing objectListing;

        do {
            objectListing = s3Client.listObjects(listObjectsRequest);
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries())
            {
                totalSize+=objectSummary.getSize();
                keys.add(objectSummary.getKey());
                System.out.println(String.format(objectSummary.getKey() + "  " +
                        "(size = " + objectSummary.getSize()/1000 + "KB)\t" +
                        "(Last Modified: " + String.format("%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS",objectSummary.getLastModified()) + ")" )
                );
            }

            listObjectsRequest.setMarker(objectListing.getNextMarker());

        } while (objectListing.isTruncated());

        long t2=System.currentTimeMillis();

        System.out.println((t2-t1)/1000 + " seconds taken for listing objects!");
        System.out.println(totalSize/1000000 + " MB in total!");
        return keys;

    }

    public List<String> getLists()
    {
        long t1=System.currentTimeMillis();
        List<String> keys=new ArrayList<String>();

        //AmazonS3Client s3Client=getS3ClientLocal();
        System.out.println("Starting");
        String bucketName="ura-data";
        //String key="submtz/DB_visits_submtz_20140426.csv";
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(bucketName);
        //.withPrefix("submtz");
        ObjectListing objectListing;

        do {
            objectListing = s3Client.listObjects(listObjectsRequest);
            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries())
            {
                keys.add(objectSummary.getKey());
                System.out.println( objectSummary.getKey() + "  " +
                        "(size = " + objectSummary.getSize()/1000 +
                        "KB)" + objectSummary.getLastModified());
            }

            listObjectsRequest.setMarker(objectListing.getNextMarker());

        } while (objectListing.isTruncated());

        long t2=System.currentTimeMillis();

        System.out.println((t2-t1)/1000 + " seconds taken for listing objects!");
        return keys;

    }

    public AmazonS3Client getS3ClientUsingKey(String accessKeyId,String secretAccessKey){
        return new AmazonS3Client(new BasicAWSCredentials(accessKeyId,secretAccessKey));

    }

    public AmazonS3Client getS3ClientLocal()
    {
        try {
            File credentials=new File("D:/aws/singtel/","credentials");
            ProfilesConfigFile configFile=new ProfilesConfigFile(credentials);
            ProfileCredentialsProvider provider=new ProfileCredentialsProvider(configFile,"default");

            AmazonS3Client s3Client=new AmazonS3Client(provider);
            return s3Client;
        }
        catch (AmazonServiceException ase) {
            ase.printStackTrace();
            System.out.println("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
            System.out.println("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }

        return null;
    }

    public AmazonS3Client getS3ClientEc2()
    {

        AmazonS3Client s3Client=new AmazonS3Client();
        return s3Client;
    }

    public void closeClient(){
        s3Client.shutdown();
    }
}

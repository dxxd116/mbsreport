package utils;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cai on 10/10/2016.
 */
public class SolrUtils {
    protected  final WKTReader reader=new WKTReader();
    protected  String env;
    protected  HashMap<Integer,Geometry> submtzs;
    public  HashMap<Integer,List<Integer>> hierarchies=new HashMap<Integer,List<Integer>>();
    private  String zkHost="52.76.76.186,52.76.80.91,52.76.58.188:2181";

    public SolrUtils(String zkHost){
        this.setZkHost(zkHost);
    }
    public SolrUtils(){}

    public static void main(String[] args) throws ParseException, SolrServerException, IOException {
        SolrUtils utils=new SolrUtils("52.76.76.186,52.76.80.91,52.76.58.188:2181");
        GeometryFactory factory=new GeometryFactory();
        Point pt=factory.createPoint(new Coordinate(103.8584722,1.290583333));

        Integer submtzId=utils.getSubmtzIdByPoint(pt);
        System.out.println("Submtz Id is "+submtzId);
        List<Integer> ids=utils.getHierarchyIds(submtzId);
        System.out.println(StringUtils.join(ids,","));
        Point pt2=factory.createPoint(new Coordinate(103.8438611,1.276638889));
        Integer submtzId2=utils.getSubmtzIdByPoint(pt2);
        System.out.println("Submtz Id is "+submtzId2);
        List<Integer> ids2=utils.getHierarchyIds(submtzId2);
        System.out.println(StringUtils.join(ids2,","));
    }

    public  void getSubmtzs() throws SolrServerException, IOException, ParseException {
        HashMap<Integer,Geometry> submtzMap=new HashMap<Integer,Geometry>();
        //WKTReader reader=new WKTReader();
        SolrClient client=getSolrClient("52.74.153.22:8983","geofeatures");

        SolrQuery qr=new SolrQuery();
        qr.add("qt","/select");
        qr.add("q","id_ti:*");
        qr.add("fq","type_s:sub-mtz");

        qr.setRows(5000);
        qr.setStart(0);
        qr.setFields("id_ti", "location_srpt");

        QueryResponse res=client.query(qr);
        SolrDocumentList docList=res.getResults();
        System.out.println(docList.size() + " results found for submtzs!");

        String location;
        Integer id;
        for (SolrDocument doc: docList)
        {
            id= (Integer) doc.getFieldValue("id_ti");
            location= (String) doc.getFieldValue("location_srpt");

            Geometry g=reader.read(location);
            submtzMap.put(id, g);

            //System.out.println(StringUtils.concat(new Object[]{id,g.getCentroid()}, " ,"));
        }


        submtzs=submtzMap;

        client.close();
    }

    public  void getSubmtzsFromCloud(String zkHost) throws SolrServerException, IOException, ParseException {
        HashMap<Integer,Geometry> submtzMap=new HashMap<Integer,Geometry>();
        //WKTReader reader=new WKTReader();
        CloudSolrClient client=getCloudSolrClient(zkHost);

        String collectionName="geofeatures";
        SolrQuery qr=new SolrQuery("id_ti:*");
        qr.add("qt","/select");
        qr.add("q","id_ti:*");
        qr.add("fq","type_s:sub-mtz");

        qr.setRows(5000);
        qr.setStart(0);
        qr.setFields("id_ti", "location_srpt");

        QueryResponse res=client.query(collectionName,qr, SolrRequest.METHOD.POST);
        SolrDocumentList docList=res.getResults();
        System.out.println(docList.size() + " results found for submtzs!");

        String location;
        Integer id;
        for (SolrDocument doc: docList)
        {
            id= (Integer) doc.getFieldValue("id_ti");
            location= (String) doc.getFieldValue("location_srpt");

            Geometry g=reader.read(location);
            submtzMap.put(id, g);

            //System.out.println(StringUtils.concat(new Object[]{id,g.getCentroid()}, " ,"));
        }


        submtzs=submtzMap;

        client.close();
    }

    public  SolrClient getSolrClient(String solrHostPort,String coreName)
    {
        String hostUrl="http://"+solrHostPort+"/solr/" + coreName;

        if (env!=null && env.equalsIgnoreCase("local")){
            hostUrl="http://localhost:8983/solr/" + coreName;
        }
        SolrClient client=new HttpSolrClient(hostUrl);

        return client;
    }

    public  ConcurrentUpdateSolrClient getConcurrentSolrClient(String solrHostPort,String coreName, int queueSize, int threadCount)
    {
        String hostUrl="http://"+solrHostPort+"/solr/" + coreName;
        if (env.equalsIgnoreCase("local")){
            hostUrl="http://localhost:8983/solr/" + coreName;
        }
        ConcurrentUpdateSolrClient client=new ConcurrentUpdateSolrClient(hostUrl,queueSize,threadCount);
        return client;
    }

    public static CloudSolrClient getCloudSolrClient(String zkhost)
    {
        //String zkhost="52.76.80.91,52.76.58.188,52.76.76.186:2181";
        //zkhost="52.65.54.15,52.65.103.71,52.65.103.73:2181";
        //SystemDefaultHttpClient cl = new SystemDefaultHttpClient();
        //CloudSolrClient solrClient=new CloudSolrClient.Builder().withZkHost(zkhost).withHttpClient(cl).build();
        CloudSolrClient solrClient=new CloudSolrClient.Builder().withZkHost(zkhost).build();
//        return solrClient;
//        CloudSolrClient client =new CloudSolrClient(zkhost);
//        client.setParallelUpdates(true);
        return solrClient;
    }


    public  void setEnv(String environ){
        env=environ;
    }

    public  String getEnv(){
        return this.env;
    }

    public  void getHierarchy() throws IOException{

        hierarchies=new HashMap<Integer,List<Integer>>();
        InputStream is=SolrUtils.class.getResourceAsStream("/submtzs.csv");
        BufferedReader br=new BufferedReader(new InputStreamReader(is));
        String line;
        while (( line=br.readLine()) !=null)
        {
            //System.out.println(line);
            String[] arr=line.split(",");
            if (arr[0].length()==0 || arr[3].equalsIgnoreCase("N.A."))
            {
                continue;
            }
            Integer submtz=Integer.valueOf(arr[0]);
            Integer subzone=Integer.valueOf(arr[3]);
            Integer planningArea=Integer.valueOf(arr[6]);
        Integer planningRegion=Integer.valueOf(arr[9]);
        List<Integer> ls=new ArrayList<Integer>();
        ls.add(subzone);
        ls.add(planningArea);
        ls.add(planningRegion);
        hierarchies.put(submtz, ls);

    }
        System.out.println(hierarchies.size() + " submtzs with hierarchy level!");
    //hierarchies.put(427, Arrays.asList(83,60,1));
        hierarchies.put(1184, Arrays.asList(0,0,0));
    //hierarchies.put(1278, Arrays.asList(329,12,2));
        hierarchies.put(1530, Arrays.asList(0,0,0));
		/*
		for (Integer s: hierarchies.keySet())
		{
			System.out.println(s + "\t"+hierarchies.get(s));
		}
		*/


        br.close();

    }

    public  Integer getSubmtzIdByPoint(Point location) throws ParseException, SolrServerException, IOException
    {
        if (submtzs==null){
            if (zkHost.isEmpty()){
                getSubmtzs();}
            else
            {
                getSubmtzsFromCloud(zkHost);
            }
        }

        Integer submtz=null;
        //Geometry lg=reader.read(location);

        for (Integer id: submtzs.keySet())
        {
            Geometry bg=submtzs.get(id);

            if (bg.covers(location))
            {
                submtz=id;

            }
        }

        return submtz;
    }

    public  Integer findSubmtzByPoint(Point location) throws ParseException, SolrServerException, IOException
    {
        if (submtzs==null)
        {
            if (zkHost.isEmpty()){
                getSubmtzs();}
            else{
                getSubmtzsFromCloud(zkHost);
            }
        }
        Integer submtz=null;
        //Geometry lg=reader.read(location);

        for (Integer id: submtzs.keySet())
        {
            Geometry bg=submtzs.get(id);

            if (bg.covers(location)){
                submtz=id;
            }
        }

        return submtz;

    }

    public  List<Integer> getHierarchyIds(Integer submtzId) throws IOException {
        if (null==submtzId){
            return Arrays.asList(-11111,-11111,-11111,-11111);
        }
        List<Integer> levelIds=new ArrayList<Integer>();
        levelIds.add(submtzId);
        if (hierarchies.isEmpty()){
            getHierarchy();
        }
        if (hierarchies.containsKey(submtzId)) {
            List<Integer> ids = hierarchies.get(submtzId);
            levelIds.addAll(ids);
        }else{
            levelIds.add(-11111);//Subzone Id
            levelIds.add(-11111);//Planning-Area Id
            levelIds.add(-11111);//Planning-Region Id
        }
        return levelIds;

    }

    public String getZkHost() {
        return zkHost;
    }

    public  void setZkHost(String zkHost) {
        this.zkHost = zkHost;
    }
}

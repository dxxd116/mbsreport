package general



import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalDate}

/**
  * Created by cai on 10/10/2016.
  */
object DateTimeTest {

  def main(args:Array[String])= {

    //getDateTime("20160908")
    //getCollectionName()
    getLocalDate();
  }

  def getCollectionName()={
    val dateTime=new DateTime(2015,9,10,1,2,20);
    val dateTimeFormatter = DateTimeFormat.forPattern("YYYYMMdd")
    val collectionName = "footfall" + dateTimeFormatter.print(dateTime)
    println(collectionName)

    val dateTimeFormatter2=DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:00'Z'")
    println(dateTimeFormatter2.print(dateTime))
  }
def getLocalDate()={
  val localDate=new LocalDate()
  println(localDate);

}


  def getDateTime(str:String)={
    val inputDateFormatter=DateTimeFormat.forPattern("yyyyMMdd");
    val startDateTime=inputDateFormatter.parseDateTime(str)
    println(startDateTime)
  }
}

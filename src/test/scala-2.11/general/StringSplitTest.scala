package general

/**
  * Created by cai on 10/26/2016 for MBSReport.
  */
object StringSplitTest {

  def main(args: Array[String]) ={
    //splitLimit()
    splitText("1.2875,103.8364444")
}
  def splitLimit()={
    val str="1|2|"
    val parts=str.split("\\|",3)
    println(parts.length)
    var sec:Int=0
    if (parts(2).isEmpty){
      sec=0
    }
    else {
      sec=parts(2).toInt
    }

    println(sec)
  }

  def splitText(txt:String)={
    val parts=txt.split(",",2)
    println(parts(1))
    parts.foreach(println)
  }

}

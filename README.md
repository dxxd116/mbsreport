# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This tools run spark local job to automate indexing of footfall collections and profile collection in Singapore platform. It contains separate classes for the creation of footfall collections,profile collections and indexing of daily footfall collection, profile collections.


### How do I get set up? ###
To set up, you need to first clone it:


```
#!git

git clone https://bitbucket.org/dxxd116/mbsreport.git <the installation directory>

```
Then cd into the directory, and run 


```
#!sbt

sbt compile <package|assembly>
```




There are several classes inside the project. Each of them is explained in details as below:

- collections.CreateFootfallCollections

```
#!java

java -cp MBSReport.jar collections.CreateFootfallColleciton <config-footfall.properties> <startDate>

```
This class create daily footfall collections for a month starting from startDate, e.g., footfall20161001, footfall20161002, ... footfall20161031


- collections.CreateProfileCollection


```
#!java

java -cp MBSReport.jar collections.CreateProfileColleciton <config-profile.properties> <collectionName>
```
This class create quarterly profile collection , like "profile-Q4-2016" as specified by <collectionName>

- indexing.FootfallSingapore


```
#!java

java -cp MBSReport.jar indexing.FootfallSingapore <config.properties> <inputFile> <collectionName>
```

This class index data from <inputFile> into the footfall collection <collectionName>
<inputFile> can be text file, s3 objects, or HDFS file available. Input file can also accept file globbing on linux system.

- indexing.ProfileLocalSingapore

```
#!java

java -cp MBSReport.jar indexing.ProfileLocalSingapore <config.properties> <Demographics FileName> <Homework FileName> <collectionName>
```
This class takes input from <Demographics FileName> and  <Homework FileName> for local profiles , and index the data into the corresponding profile collection as specified by <collectionName>,e.g., profile-Q1-2017.

<inputFile> can be text file, s3 objects, or HDFS file available. Input file can also accept file globbing on linux system.

- indexing.ProfileVisitorSingapore

```
#!java

java -cp MBSReport.jar indexing.ProfileVisitorSingapore <config.properties>  <Visitor FileName> <collectionName>
```

This class indexing visitor profiles into the same quarterly collection as local profiles, e.g., profile-Q1-2017.

<inputFile> can be text file, s3 objects, or HDFS file available. Input file can also accept file globbing on linux system.

The config files can be modified as necessary. Most important config settings are:

1. nodeset  -  The solr nodes where the new collections will be created on. 

2. zkhost   -  The zookeeper address of solr Cloud


####Miscellaneous###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

name := "MBSReport"

version := "1.2"

scalaVersion := "2.11.8"

mainClass in assembly:=Some("report.algo.FileProcessor")

// https://mvnrepository.com/artifact/org.apache.solr/solr-solrj
libraryDependencies += "org.apache.solr" % "solr-solrj" % "6.2.0"
// https://mvnrepository.com/artifact/joda-time/joda-time
libraryDependencies += "joda-time" % "joda-time" % "2.9.4"
// https://mvnrepository.com/artifact/com.vividsolutions/jts
libraryDependencies += "com.vividsolutions" % "jts" % "1.13"
// https://mvnrepository.com/artifact/org.apache.commons/commons-lang3
libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.0"

//libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.4.11"

libraryDependencies += "net.debasishg" %% "redisclient" % "3.2"

libraryDependencies += "com.github.seratch" %% "awscala" % "0.5.+"

// https://mvnrepository.com/artifact/org.apache.spark/spark-core_2.10
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.0.0"

// https://mvnrepository.com/artifact/com.typesafe.play/play-json_2.11
//libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.9"

// https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-scala_2.10
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.6.5"


assemblyJarName in assembly :="MBSReport.jar"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case PathList("META-INF", xs @ _*) => MergeStrategy.filterDistinctLines
  case x => MergeStrategy.first
}